# Requirements

Node Version
This repository is only tested to work against 12.22.3, as versions higher than 12.x require an update to an upstream HID library.

Follow the node-gyp requirement guide
link: https://github.com/nodejs/node-gyp#on-windows

From Administrator Prompt
`npm install --global windows-build-tools@4.0.0`

If that fails, you can install chocolatey and run `choco upgrade python2 visualstudio2017-workload-vctools` (also from administrator prompt)

From Regular Prompt
`npm install`
