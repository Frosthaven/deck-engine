/**
 * Class representing a rate limited queue
 * @alias _.RateLimitedQueue
 */
class RateLimitedQueue {
  /**
   * Creates a rate limited queue of units by which actions can be performed on
   * @param {object} params a group of parameters used to create the queue
   * @param {number} params.units how many units we are handling per a given time window
   * @param {number} params.per the time window in milliseconds for which units are handled
   * @param {function} params.action a handler that does something when a unit is ready
   */
  constructor(params) {
    // provided parameters
    this.units  = params.units;
    this.per    = params.per;
    this.action = params.action;
    this.max    = params.max || 0;

    // other parameters
    this.queue         = [];
    this.task          = false;
    this.rate          = Math.ceil(this.per / this.units);
    this.spindown_rate = 500; // this is a workaround to allow last-second entries to process
  }

  /**
   * Gets the size of the queue
   */
  size() {
    return this.queue.length
  }

  /**
   * Removes oldest units from the queue if max was specified
   */
  removeOldestUnits() {
    if (this.max > -1 && this.queue.length > this.max) {
      this.queue = this.queue.slice(-this.max)
    }
  }

  /**
   * Processes the next unit in the queue
   */
  processUnit() {
    if (this.queue.length > 0 && typeof this.action === 'function') {
      let unit = this.queue.shift();
      this.action(unit);
    } else {
        // wait for the spindown rate and then close the queue
        setTimeout((x) => {
            this.queue.length === 0 ? this.stop() : null;
        }, this.spindown_rate);
    }
  }

  /**
   * Starts the queue if it isn't already running
   */
  start() {
    if (!this.task) {
      // start the first unit immediately
      // @todo this causes repeated presses to exceed rate limit by recreating
      // the queue and running instantly every time...
      this.processUnit()

      // queue the remaining units to the queue
      this.task = setInterval(() => {
        this.processUnit()
      }, this.rate);
    }
  }

  /**
   * Stops the queue and clears running data
   */
  stop() {
    // we do this after a rate timeout to ensure future queue starts do not
    // restart faster and begin running faster than the defined rate limit
    setTimeout(() => {
      clearInterval(this.task);
      this.task = false;
    }, this.rate)
  }

  /**
   * Adds something to the queue for processing, and attempts to start it
   * @param {*} unit anything that can be added to an array
   */
  add(unit) {
    this.removeOldestUnits()
    this.queue.push(unit);
    this.start();
  }
};

module.exports = RateLimitedQueue;
