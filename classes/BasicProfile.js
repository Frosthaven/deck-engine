class BasicProfile {
  constructor(config, sys, lib, profileName) {
    this.config       = config
    this.sys          = sys
    this.lib          = lib
    this.sequences    = {}
    this._profileName = profileName
  }

  setup() {

  }

  buttons() {

  }

  registerSequence = (buttonIndex, sequence, defaultIndex, callback) => {
    // @todo make it easier for a profile to create a sequencing button
    // console.log(sys.actionRegistry[buttonIndex])
    console.log(this.sequences[buttonIndex])
    this.sequences = this.sequences || {}
    this.sequences[buttonIndex] = this.sequences[buttonIndex] || {}

    this.sequences[buttonIndex].list = this.sequences[buttonIndex].list || sequence
    this.sequences[buttonIndex].lastIndex = this.sequences[buttonIndex].lastIndex || defaultIndex
    this.sequences[buttonIndex].callback = this.sequences[buttonIndex].callback || callback
  }

  sequence = (buttonIndex) => {
    let maxIndex = this.sequences[buttonIndex].list.length - 1
    let sequence = this.sequences[buttonIndex]
    sequence.lastIndex++
    if (sequence.lastIndex > maxIndex) {
      sequence.lastIndex = 0
    }
    this.sequences[buttonIndex].callback(sequence.lastIndex, sequence.list[sequence.lastIndex])
  }
};

module.exports = BasicProfile;
