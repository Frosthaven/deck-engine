module.exports = {
  apps : [{
    name: "streamdeck",
    interpreter : 'node@12.22.3',
    script: "./index.js",
    autorestart: true,
    error: "./logs/error.log",
    out: "./logs/out.log",
    watch: true,
    ignore_watch: [
      "cache",
      "logs"
    ],
    env: {
      NODE_ENV: "development",
    },
    env_production: {
      NODE_ENV: "production",
    }
  }]
}
