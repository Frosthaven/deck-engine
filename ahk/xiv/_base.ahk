#UseHook
#SingleInstance force

SetKeyDelay 150
SetControlDelay 1000
CoordMode, Mouse, Screen
CoordMode, ToolTip, Screen

; TOOLTIP LIBRARY**************************************************************
;******************************************************************************

ToolTipFont(Options := "", Name := "", hwnd := "") {
    static hfont := 0
    if (hwnd = "")
        hfont := Options="Default" ? 0 : _TTG("Font", Options, Name), _TTHook()
    else
        DllCall("SendMessage", "ptr", hwnd, "uint", 0x30, "ptr", hfont, "ptr", 0)
}
 
ToolTipColor(Background := "", Text := "", hwnd := "") {
    static bc := "", tc := ""
    if (hwnd = "") {
        if (Background != "")
            bc := Background="Default" ? "" : _TTG("Color", Background)
        if (Text != "")
            tc := Text="Default" ? "" : _TTG("Color", Text)
        _TTHook()
    }
    else {
        VarSetCapacity(empty, 2, 0)
        DllCall("UxTheme.dll\SetWindowTheme", "ptr", hwnd, "ptr", 0
            , "ptr", (bc != "" && tc != "") ? &empty : 0)
        if (bc != "")
            DllCall("SendMessage", "ptr", hwnd, "uint", 1043, "ptr", bc, "ptr", 0)
        if (tc != "")
            DllCall("SendMessage", "ptr", hwnd, "uint", 1044, "ptr", tc, "ptr", 0)
    }
}
 
_TTHook() {
    static hook := 0
    if !hook
        hook := DllCall("SetWindowsHookExW", "int", 4
            , "ptr", RegisterCallback("_TTWndProc"), "ptr", 0
            , "uint", DllCall("GetCurrentThreadId"), "ptr")
}
 
_TTWndProc(nCode, _wp, _lp) {
    Critical 999
   ;lParam  := NumGet(_lp+0*A_PtrSize)
   ;wParam  := NumGet(_lp+1*A_PtrSize)
    uMsg    := NumGet(_lp+2*A_PtrSize, "uint")
    hwnd    := NumGet(_lp+3*A_PtrSize)
    if (nCode >= 0 && (uMsg = 1081 || uMsg = 1036)) {
        _hack_ = ahk_id %hwnd%
        WinGetClass wclass, %_hack_%
        if (wclass = "tooltips_class32") {
            ToolTipColor(,, hwnd)
            ToolTipFont(,, hwnd)
        }
    }
    return DllCall("CallNextHookEx", "ptr", 0, "int", nCode, "ptr", _wp, "ptr", _lp, "ptr")
}
 
_TTG(Cmd, Arg1, Arg2 := "") {
    static htext := 0, hgui := 0
    if !htext {
        Gui _TTG: Add, Text, +hwndhtext
        Gui _TTG: +hwndhgui +0x40000000
    }
    Gui _TTG: %Cmd%, %Arg1%, %Arg2%
    if (Cmd = "Font") {
        GuiControl _TTG: Font, %htext%
        SendMessage 0x31, 0, 0,, ahk_id %htext%
        return ErrorLevel
    }
    if (Cmd = "Color") {
        hdc := DllCall("GetDC", "ptr", htext, "ptr")
        SendMessage 0x138, hdc, htext,, ahk_id %hgui%
        clr := DllCall("GetBkColor", "ptr", hdc, "uint")
        DllCall("ReleaseDC", "ptr", htext, "ptr", hdc)
        return clr
    }
}

; FUNCTION LIBRARY*************************************************************
;******************************************************************************

displayLoadTooltip() {
    ToolTipFont("s40", "Arial")
    ToolTipColor("Black", "Green")
    ToolTip,MACRO READY,0,0
    sleep 900
    ToolTip

    return
}

showTooltipMsg(msg) {
    ToolTipFont("s40", "Arial")
    ToolTipColor("Black", "White")
    Tooltip,%msg%,0,0
    return
}

showTooltip(craftCurrent, craftTotal, timeLeft) {
    ToolTipFont("s40", "Arial")
    ToolTipColor("Black", "White")
    Tooltip,%craftCurrent%/%craftTotal% - %timeLeft% minute(s) left,0,0
    return
}

hideTooltip() {
    Tooltip
    return
}

XIVWindowIsNotActive() {
    IfWinNotActive, FINAL FANTASY XIV
        return true
    return false
}

activateXIVWindow() {
    if WinExist("FINAL FANTASY XIV")
        WinActivate

    return
}

getXIVWindowID() {
    activateXIVWindow()
    Return WinExist("FINAL FANTASY XIV")
}

XIVWindowExists() {
    Return WinExist("FINAL FANTASY XIV")
}

requireXIVRunning(RunWith) {
    fn := Func("closeEverything").bind(RunWith)
    SetTimer, %fn%, 5000

    return
}

startOtherApps(RunWith) {
    for index, AppPath in RunWith
    {
        SplitPath, AppPath, App
        Process, Exist, %App%
        If !ErrorLevel
            Run % AppPath
    }

    activateXIVWindow()
    return
}

closeOtherApps(RunWith) {
    for index, AppPath in RunWith
    {

        SplitPath, AppPath, App
        Process, Exist, %App%
        If ErrorLevel
            Process, Close, %ErrorLevel%
    }

    return
}

closeEverything(RunWith) {
    if XIVWindowExists()
        return

    closeOtherApps(RunWith)
    ExitApp
    Return
}



launchOrFocusApp(pName, pLink) {
    DetectHiddenWindows, On
    Process, Exist, %pName%
    IfWinNotExist WinTitle ahk_pid %ErrorLevel%
    Run % pLink
    Else
    DetectHiddenWindows, Off
    IfWinNotExist WinTitle ahk_pid %ErrorLevel%
    {
        WinShow, WinTitle ahk_pid %ErrorLevel%
        WinActivate, WinTitle ahk_pid %ErrorLevel%
    }
    Else
    IfWinExist WinTitle ahk_pid %ErrorLevel%
    {
        WinMinimize, WinTitle ahk_pid %ErrorLevel%
        WinHide, WinTitle ahk_pid %ErrorLevel%
    }
    Return
}