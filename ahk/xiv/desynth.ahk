#NoTrayIcon
#include %A_ScriptDir%/_base.ahk

if (XIVWindowIsNotActive() == true) {
	ExitApp
	Return
}

; SCRIPT **********************************************************************
;******************************************************************************

; as how many times to desynth
InputBox, desynthcount, Desynth Count, How many would you like to desynth?
If ErrorLevel !=1
{
	If desynthcount < 1
	{
		Return
	}

	;get the xiv window id
	ID := getXIVWindowID()

	;calculate time required
	pause_wait_ready := 300
	pause_wait_results := 3000
	totaltime := (pause_wait_ready + (A_KeyDelay * 5) + pause_wait_results)
	totaltime := Round(((totaltime/1000)/60) * desynthcount,1)

	;final messagebox alerting time remaining
	MsgBox,, Now Reducing, Now reducing %desynthcount% units over %totaltime% minute(s). Press OK to continue or ctrl+x to cancel.

	;run desynthesis
	ControlSend,,{right},ahk_id %ID%
	Loop %desynthcount%
	{

		sleep % pause_wait_ready
		ControlSend,,{right},ahk_id %ID%
		ControlSend,,{home},ahk_id %ID%
		ControlSend,,{right},ahk_id %ID%
		ControlSend,,{home},ahk_id %ID%
		sleep % pause_wait_results
		ControlSend,,{right},ahk_id %ID%
		ControlSend,,{home},ahk_id %ID%
	}	
}
Return

; CANCEL SCRIPT ***************************************************************
;******************************************************************************

^!+x::
	ExitApp
	Return