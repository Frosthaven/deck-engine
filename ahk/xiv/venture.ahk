#include %A_ScriptDir%/_base.ahk
#SingleInstance force

global retainer_count := 1
global venture_count := 1
global elapsed := 0
global processed := 0

if (XIVWindowIsNotActive() == true) {
	ExitApp
	Return
}

BlockInput, MouseMove
;confirm retainer count
InputBox, retainer_count_input, Auto-Venture,Enter the number of retainers you have`n`nHit escape to cancel.
retainer_count := retainer_count_input
If ErrorLevel != 1
{
	if retainer_count <= 0
	{
		ExitAutomation()
	}

	;confirm venture count
	InputBox, venture_count_input, Session Count,How many venture sessions do you want to automate?`n`nHit escape to cancel.
	venture_count := venture_count_input
	If ErrorLevel != 1
	{
		if venture_count <= 0
		{
			ExitAutomation()
		}
		;confirm possible wait time
		InputBox, minutes_until_ready_input, Starting Time,How many minutes until the ventures are ready?`n`nLeave blank if they are ready now`n`nHit escape to cancel.
		If ErrorLevel != 1
		{
			if minutes_until_ready_input > 0
			{
				;we specified that the ventures weren't ready yet
				if minutes_until_ready_input > 60
				{
					;prevent delay from being greater than quick venture max
					minutes_until_ready_input = 60
				}

				;adjust elapsed time forward in relation to the provided delay
				elapsed := 3600000 - (minutes_until_ready_input*60000)

				;schedule the venture
				SetTimer, UpdateCheck, 1000
				BlockInput, MouseMoveOff
				Return
			}
			else
			{
				;there was no delay specified, so we can do the initial venture
				DoVentures(1,0)
				BlockInput, MouseMoveOff
				Return
			}
		}
		else
		{
			ExitAutomation()
		}
	}
	else
	{
		ExitAutomation()
	}
}
else
{
	ExitAutomation()
}
Return

; LABELS **********************************************************************
;******************************************************************************
UpdateCheck:
	elapsed := elapsed + 1000
	remain := 3600000 - elapsed
	remain_stamp := MillisecToTime(remain)
	msg := "[" processed "/" venture_count "] Next Ventures in: " remain_stamp " (Press Ctrl+Alt+V to end)"
	showTooltipMsg(msg)

	if venture_count <= 0
	{
		ExitAutomation()
		Return
	}

	if processed >= %venture_count%
	{
		ExitAutomation()
		Return
	}

	if elapsed >= 3600000
	{
		SetTimer, UpdateCheck, off
		elapsed := 0

		DoVentures(0,1)
	}
	Return

; SCRIPT **********************************************************************
;******************************************************************************
ExitAutomation() {
	hideTooltip()
	BlockInput, MouseMoveOff
	ExitApp
	Return
}

MillisecToTime(msec) {
	secs := Floor(Mod(msec / 1000, 60))
	mins := Floor(Mod(msec / (1000 * 60), 60))
	Return, Format("{:02}:{:02}", mins, secs)
}

DoVentures(do_initial, do_resend) {
	if venture_count <= 0
	{
		ExitAutomation()
		Return
	}

	if processed >= %venture_count%
	{
		ExitAutomation()
		Return
	}

	msg := "[" processed "/" venture_count "] Automating Ventures (Press Ctrl+Alt+V to end)"
	showTooltipMsg(msg)
	BlockInput, MouseMove
	If retainer_count > 0
	{
		ID := getXIVWindowID()
		pause_dialog := 1200
		current_index := 1

		BlockInput, MouseMove
		Loop %retainer_count%
		{
			;move to activate the cursor mode
			ControlSend,,{left},ahk_id %ID%

			;navigate to the next retainer position
			navigate_count := current_index - 1
			Loop %navigate_count%
			{
				ControlSend,,{down},ahk_id %ID%
			}

			
			ControlSend,,{home},ahk_id %ID%
			Sleep % pause_dialog
			ControlSend,,{home},ahk_id %ID%
			Sleep % pause_dialog

			;process the action
			if do_resend > 0
			{
				;resend completed ventures
				ControlSend,,{up},ahk_id %ID%
				ControlSend,,{up},ahk_id %ID%
				ControlSend,,{up},ahk_id %ID%
				ControlSend,,{up},ahk_id %ID%
				ControlSend,,{up},ahk_id %ID%
				ControlSend,,{home},ahk_id %ID%
				Sleep % pause_dialog
				ControlSend,,{left},ahk_id %ID%
				ControlSend,,{home},ahk_id %ID%
				Sleep % pause_dialog
				ControlSend,,{left},ahk_id %ID%
				ControlSend,,{home},ahk_id %ID%
				Sleep % pause_dialog
				ControlSend,,{home},ahk_id %ID%
			}
			
			if do_initial > 0
			{
				;start quick ventures
				ControlSend,,{up},ahk_id %ID%
				ControlSend,,{up},ahk_id %ID%
				ControlSend,,{up},ahk_id %ID%
				ControlSend,,{up},ahk_id %ID%
				ControlSend,,{home},ahk_id %ID%
				Sleep % pause_dialog
				ControlSend,,{up},ahk_id %ID%
				ControlSend,,{up},ahk_id %ID%
				ControlSend,,{home},ahk_id %ID%
				Sleep % pause_dialog
				ControlSend,,{left},ahk_id %ID%
				ControlSend,,{home},ahk_id %ID%
				Sleep % pause_dialog
				ControlSend,,{home},ahk_id %ID%
			}

			Sleep % pause_dialog
			ControlSend,,{escape},ahk_id %ID%
			Sleep % pause_dialog
			ControlSend,,{home},ahk_id %ID%
			Sleep % pause_dialog

			;incriment our index for the main menu
			current_index := current_index + 1
		}
		processed := processed + 1
		SetTimer, UpdateCheck, 1000
		BlockInput, MouseMoveOff
	}
	else
	{
		ExitAutomation()
	}
	BlockInput, MouseMoveOff
	Return
}