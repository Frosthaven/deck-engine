#NoTrayIcon
#include %A_ScriptDir%/_base.ahk

if (XIVWindowIsNotActive() == true) {
	ExitApp
	Return
}

; SCRIPT **********************************************************************
;******************************************************************************

; ask how many to craft
BlockInput, MouseMove
InputBox, craftcount, Craft Count, How many would you like to craft?`n`n     Hit escape to cancel.
If ErrorLevel != 1
{
	If craftcount < 1
	{
		BlockInput, MouseMoveOff
		Return
	}

	InputBox, macro1sleep, Macro Timer 1, How many seconds does the macro take?`n`n     Add together all of the <wait> timers.`n`n     Hit escape to cancel.
	If ErrorLevel = 1
	{
		BlockInput, MouseMoveOff
		Return
	}
	
	InputBox, whichMacro, Which macro?, Which macro do you want to use (1 2 3)?`n`n     Hit escape to cancel.
	If ErrorLevel = 1
	{
		BlockInput, MouseMoveOff
		Return
	}

	;calculate total time required
	pause_wait_actions_ready := 1300
	pause_return_menu := 1900
	pause_msgbox_ended := 1200

	totaltime := ((macro1sleep * 1000) + pause_return_menu + pause_wait_actions_ready) + A_KeyDelay
	if macro1sleep > 0
	{
		totaltime := totaltime + A_KeyDelay
	}
	if macro2sleep > 0
	{
		totaltime := totaltime + A_KeyDelay
	}

	timeperunit := Round((totaltime/1000)/60,1)
	totaltime := Round(((totaltime/1000)/60) * craftcount,1)

	;final messagebox alerting time remaining
	MsgBox,, Now Crafting, Now crafting %craftcount% units over %totaltime% minute(s). Press OK to continue or escape to cancel. You can interrupt the craft by htiting ctrl+alt+c again.

	;give the msgbox time to vanish just in case
	sleep % pause_msgbox_ended

	;get the xiv window id
	ID := getXIVWindowID()
	COUNT := 0
	Loop %craftcount%
	{
		COUNT++
		showTooltip(COUNT, craftcount, Round((craftcount - COUNT + 1) * timeperunit,1))

		; confirm synthesize button
		ControlSend,,{home},ahk_id %ID%
		BlockInput, MouseMoveOff

		;wait for synth menu to be ready
		sleep % pause_wait_actions_ready

		;macro 1
		If macro1sleep > 0
		{
			if whichMacro = 1
			{
				ControlSend,,{ctrl down}{3}{ctrl up},ahk_id %ID%
			}
			if whichMacro = 2
			{
				ControlSend,,{ctrl down}{6}{ctrl up},ahk_id %ID%
			}
			if whichMacro = 3
			{
				ControlSend,,{ctrl down}{9}{ctrl up},ahk_id %ID%
			}
			sleep % macro1sleep * 1000
		}

		BlockInput, MouseMove

		;wait for menu to appear
		sleep % pause_return_menu

		;prepare cursor position
		ControlSend,,{right},ahk_id %ID%
		ControlSend,,{home},ahk_id %ID%
	}
	BlockInput, MouseMoveOff
	hideTooltip()
}
BlockInput, MouseMoveOff