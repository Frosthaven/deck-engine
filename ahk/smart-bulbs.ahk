#NoTrayIcon
#Include C:/Standalone/autohotkey-scripts/yeelight-controller/yeelight.ahk

global yeelights := {}
global profiles := {}
global lookup := {}
global activeProfile := ""

profile(thatName, thatHotkey, dotColor, yee_mode, yee_a, yee_b, yee_c, yee_d)
{
	;store the data
	yeelights[thatName] := {"mode":yee_mode,"p1":yee_a,"p2":yee_b,"p3":yee_c,"p4":yee_d}
	Return
}

profile("CRGB_Hades_Zagreus",	"^!Numpad0",	"red",			"color",		"0xff1300",	"0xFFAD00",	70,		40)
profile("CRGB_Hades_Zeus",		"^!Numpad1",	"yellow",		"color",		"0xffef00",	"0xffffff",	70,		40)
profile("CRGB_Hades_Poseidon",	"^!Numpad2",	"blue",			"color",		"0x000cff",	"0x00fff2",	70,		60)
profile("CRGB_Hades_Aphrodite",	"^!Numpad3",	"pink",			"color",		"0xff00b4",	"0xFF00F8",	70,		50)
profile("CRGB_Hades_Dionysus",	"^!Numpad4",	"purple",		"color",		"0x5a00ff",	"0x35ff6e",	20,		15)
profile("CRGB_Hades_Artemis",	"^!Numpad5",	"green",		"color",		"0x00ffa4",	"0x79ff00",	70,		50)
profile("CRGB_Hades_Chaos",		"^!Numpad6",	"rainbow",		"color",		"0x8A00FF",	"0xff0015",	45,		55)
profile("CRGB_Hades_Megaera",	"^!Numpad7",	"cyan-magenta",	"color",		"0x0000ff",	"0xff004c",	70,		70)
profile("CRGB_Hades_Tisiphone",	"^!Numpad8",	"green",		"color",		"0x7bff00",	"0x7600FF",	35,		45)
profile("CRGB_Hades_Alecto",	"^!Numpad9",	"red",			"color",		"0xff0015",	"0x7bff00",	55,		35)
profile("CRGB_Hades_Hermes",	"^!NumpadMult", "orange",		"color",		"0xbf0027",	"0xFF9200",	90,		70)
profile("CRGB_Hades_Skelly",	"^!NumpadAdd",	"white",		"temperature",	6500,		50,			0,		0)
profile("CRGB_Hades_Thanatos",	"^!NumpadSub",	"black",		"off",			0,			0,			0,		0)





updateYeelights(profileName)
{
	if (profileName = "getProfile") {
		RegRead, profileName,  HKEY_CURRENT_USER\Software\Corsair\CORSAIR iCUE Software, ActiveProfile
	}

	;change yeelights
	yee_mode := yeelights[profileName]["mode"]
	yee_a := yeelights[profileName]["p1"]
	yee_b := yeelights[profileName]["p2"]
	yee_c := yeelights[profileName]["p3"]
	yee_d := yeelights[profileName]["p4"]

	if (yee_mode = "color") {

		;MsgBox % yeelight_mode . " " . yeelight_param1 . " " . yeelight_param2 . " " . yeelight_param3 . " " . yeelight_param4
		;1 = color a, 2 = color b, 3 = brightness a, 4 = brightness b
		setColor(dev["lamp"], yee_b, yee_d)
		setColor(dev["ceiling_rear"], yee_b, yee_d)
		setColor(dev["lantern"], yee_a, yee_c)
		setColor(dev["ceiling_pantry"], yee_a, yee_c)
		setColor(dev["ceiling_desk"], yee_a, yee_c)
	} else if (yee_mode = "temperature") {
		;1 = kelvin, 2 = brightness
		setTemperature(dev["lantern"], yee_a, yee_b)
		setTemperature(dev["ceiling_pantry"], yee_a, yee_b)
		setTemperature(dev["ceiling_rear"], yee_a, yee_b)
		setTemperature(dev["lamp"], yee_a, yee_b)
		setTemperature(dev["ceiling_desk"], yee_a, yee_b)
	} else if (yee_mode = "off") {
		allOff()
	}
	return
}





Switch %0%
{
	case "ceiling-off":
		setOff(dev["ceiling_desk"])
		setOff(dev["ceiling_pantry"])
		setOff(dev["ceiling_rear"])
		return
	case "ceiling-on":
		setOn(dev["ceiling_desk"])
		setOn(dev["ceiling_pantry"])
		setOn(dev["ceiling_rear"])
		return
	case "off":
		allOff()
		return
	case "low":
		for index, value in dev
			if (index = "lantern") {
				setTemperature(value, 6500, 50)
			} else if (index = "lamp") {
				setTemperature(value, 6500, 50)
			} else {
				setTemperature(value, 6500, 0)
			}
		return
	case "medium":
		for index, value in dev
			if (index = "lantern") {
				setColor(value, "0x7c66cc", 65)
			} else {
				setColor(value, "0x7c66cc", 65)
			}
		return
	case "high":
		for index, value in dev
			if (index = "ceiling_desk") {
				setTemperature(value, 6500, 100)
			} else if (index = "ceiling_pantry") {
				setTemperature(value, 6500, 100)
			} else if (index = "ceiling_rear") {
				setTemperature(value, 6500, 100)
			} else {
				setColor(value, "0x7a66cc", 100)
			}
		return
	case "rgb":
		updateYeelights("getProfile")
		return
	case "mom":
		for index, value in dev
			if (index = "lantern" or index = "lamp") {
				setOff(value)
			} else if (index = "ceiling_desk") {
				setTemperature(value, 6500, 30)
			} else {
				setTemperature(value, 6500, 1)
			}
		return
}
