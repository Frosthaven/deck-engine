﻿#NoTrayIcon

;get a list of all profiles
get_icue_profiles(Directory)
{
	static FileArr := []
	files =
	Loop %Directory%\*.*
	{
		;now only match the ones that end in .exe and dont start with scimitar
		RegExMatch(Path, "\w+$", Extension)
		FirstNine := SubStr(A_LoopFileName, 1, 9)
		if (Extension is "cueprofile") {
			Splitpath, A_LoopFileName, fname, fdir, fext, fnamene, fdrv
			FileArr.Push(fnamene)
		}
	}
	return FileArr
}

HasVal(haystack, needle) {
	if !(IsObject(haystack)) || (haystack.Length() = 0)
		return 0
	for index, value in haystack
		if (value = needle)
			return index
	return 0
}

RegRead, LastProfile,  HKEY_CURRENT_USER\Software\Corsair\CORSAIR iCUE Software, ActiveProfile
Profiles := get_icue_profiles("C:\ProgramData\Corsair\CUE4\GameSdkEffects\profiles")
Sort, Profiles
NextProfile := HasVal(Profiles,LastProfile)+1
if (Profiles.HasKey(NextProfile) and NextProfile > 0) {
	NextProfile := Profiles[NextProfile]
} else {
	NextProfile := Profiles[1]
}
RegWrite REG_SZ, HKEY_CURRENT_USER\Software\Corsair\CORSAIR iCUE Software, ActiveProfile, %NextProfile%
