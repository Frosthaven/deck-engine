"use strict";
/* DECK LAYOUT

    0   1   2   3   4
    5   6   7   8   9
    10  11  12  13  14

*/

/* REQUIREMENTS ***************************************************************
 ******************************************************************************/
const fs = require("fs");
const path = require("path");
const sharp = require("sharp");
const activeWin = require("active-win");
const winAudio = require("win-audio");
const speaker = winAudio.speaker;
const { openStreamDeck } = require("elgato-stream-deck");
const child = require("child_process").execFile;
const EventEmitter = require("events");
const Voicemeeter = require("voicemeeter-connector").Voicemeeter;

global.use = (fPath) => {
    return require("./" + fPath);
};

/* USER CONFIG ****************************************************************
 ******************************************************************************/
let config = {};
config.defaultProfile = "control";
config.profilePollingRate = 250;
config.autoSwapToProfiles = false;
config.saveLastPosition = true;
config.autohotkey_path = path.normalize(`./bin/AutoHotkey.exe`);
config.vm = {
    enabled: true,
    mic: {
        type: "Strip",
        index: 0,
    },
    speaker: {
        type: "Bus",
        index: 1,
    },
    browser: {
        type: "Strip",
        index: 2,
        mixTo: "B1",
    },
    music: {
        type: "Strip",
        index: 3,
        mixTo: "B1",
    },
};

/* SYSTEM VARIABLES ************************************************************
 ******************************************************************************/
let sys = {};

sys.vm = null;
if (config.vm.enabled) {
    Voicemeeter.init()
        .then(async (vm) => {
            // Connect to your voicemeeter client
            if (vm) {
                sys.vm = vm;
                sys.vm.connect();
            }
        })
        .catch((e) => console.log);
}
sys.isVoiceMeterEnabled = () => {
    return (
        null !== config.vm.enabled &&
        null !== sys.vm &&
        null !== sys.vm.setParameter
    );
};

//catches uncaught exceptions
process.on("uncaughtException", exitHandler.bind(null, { exit: true }));

sys.events = new EventEmitter();
sys.deck = openStreamDeck();
sys.deck.clearAllKeys();
sys.classes = {};

sys.actionRegistry = [];
sys.offsetRegistry = [];
sys.tasks = {};
sys.zones = {};

sys.profiles = {};
sys.lastProcess = null;
sys.lastProfile = null;
sys.lastOffset = 0;

/* FUNCTIONS ******************************************************************
 ******************************************************************************/
let lib = {};

lib.registerTask = (name, ms, callback) => {
    const timer = ms;

    if (sys.tasks[name]) {
        // fail silently for now since plugins will attempt to
        // reregister tasks on reconnect
    } else {
        sys.tasks[name] = {
            interval: ms,
            action: setInterval(function () {
                process.nextTick(function () {
                    callback();
                });
            }, timer),
        };
        callback();
    }
};

lib.sleep = (ms) => {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
};

lib.ahk = (filename, extraparams) => {
    const filePath = path.normalize(`./ahk/${filename}.ahk`);
    let parameters = [filePath];

    if (extraparams) {
        parameters.push.apply(parameters, extraparams);
    }
    child(config.autohotkey_path, parameters, function (error, data) {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
    });
};

lib.defineZone = (name, buttonIndexes) => {
    sys.zones[name] = buttonIndexes;
};

lib.defineProfiles = () => {
    sys.profiles = {};

    fs.readdir(path.normalize("./profiles"), (err, items) => {
        items.forEach((file) => {
            if (file.endsWith(".js")) {
                let profileName = file.replace(".js", "");
                let classObject = require(path.normalize(
                    `${__dirname}/profiles/${file}`
                ));
                let profile = new classObject(config, sys, lib, profileName);
                sys.classes[profileName] = profile;
                sys.classes[profileName].setup();
                sys.profiles[profileName] = profile.buttons();
            }
        });
        sys.events.emit("ready");
    });
};

lib.getZonePagingSteps = (zoneName) => {
    return sys.zones[zoneName].length;
};

lib.assignProfileToZone = (
    profileName,
    zoneName,
    offset = 0,
    isTab = false
) => {
    const toProfile = sys.profiles[profileName] ? profileName : null;
    if (toProfile !== sys.lastProfile) {
        offset = 0;
    }

    // change the profile
    if (toProfile) {
        // decide new offset
        let newOffset =
            config.saveLastPosition &&
            sys.offsetRegistry[toProfile] &&
            toProfile !== sys.lastProfile
                ? sys.offsetRegistry[toProfile]
                : sys.lastOffset + offset;

        // define boundaries for paging
        const bounds = {
            lower: 0,
            upper: sys.profiles[toProfile].length,
        };

        // cap our offsets
        if (newOffset > bounds.upper) {
            let numPages = Math.ceil(
                bounds.upper / lib.getZonePagingSteps(zoneName)
            );
            newOffset =
                numPages * lib.getZonePagingSteps(zoneName) -
                lib.getZonePagingSteps(zoneName);
        }
        if (newOffset < bounds.lower) {
            newOffset = bounds.lower;
        }

        // cancel processing if the page doesn't change and we aren't specifically using
        // the tab button which forces "return to top" behavior
        if (
            toProfile === sys.lastProfile &&
            newOffset === sys.lastOffset &&
            !isTab
        ) {
            return;
        }

        // set the buttons up in the zone
        const zone = sys.zones[zoneName];
        if (sys.profiles[toProfile]) {
            let currentKey = newOffset;
            zone.forEach((buttonIndex, index) => {
                lib.clearButton(buttonIndex);
                if (
                    sys.profiles[toProfile] &&
                    sys.profiles[toProfile][currentKey]
                ) {
                    lib.setButton(
                        buttonIndex,
                        sys.profiles[toProfile][currentKey],
                        toProfile
                    );
                }
                currentKey++;
            });
        }

        // save offsets for later
        sys.lastOffset = newOffset;
        sys.lastProfile = toProfile;

        sys.offsetRegistry[toProfile] = newOffset;

        // trigger profile change functions
        sys.actionRegistry.forEach((otherKeyIndex) => {
            if (
                otherKeyIndex &&
                typeof otherKeyIndex.profilechange == "function"
            ) {
                otherKeyIndex.profilechange();
            }
        });

        // remove all existing timers
        sys.actionRegistry.forEach(lib.clearState);
    }
};

lib.clearState = (keyRegistry) => {
    if (keyRegistry && keyRegistry._state) {
        if (keyRegistry._state.timer) {
            try {
                clearInterval(keyRegistry._state.timer);
            } catch (err) {
                // do nothing
            }
            delete keyRegistry._state.timer;
        }
        delete keyRegistry._state;
    }
};

lib.scanActiveWindow = () => {
    let activeWindow = (async () => {
        let window = await activeWin();
        if (window && window.owner && window.owner.name) {
            let path = window.owner.path;
            let exeName = window.owner.name;
            if (exeName !== sys.lastProcess) {
                sys.lastProcess = exeName;

                if (config.autoSwapToProfiles && sys.profiles[exeName]) {
                    lib.assignProfileToZone(exeName, "profile");
                } else if (config.autoSwapToProfiles) {
                    sys.lastProcess = exeName;
                    lib.assignProfileToZone(config.defaultProfile, "profile");
                }

                // trigger window change functions
                sys.actionRegistry.forEach((otherKeyIndex) => {
                    if (
                        otherKeyIndex &&
                        typeof otherKeyIndex.windowchange == "function"
                    ) {
                        otherKeyIndex.windowchange(window);
                    }
                });
            }
        }
    })();
};

lib.setImage = (buttonIndex, imgPath) => {
    if (imgPath) {
        sharp(path.resolve(imgPath))
            .flatten() // eliminate alpha channel
            .resize(sys.deck.ICON_SIZE, sys.deck.ICON_SIZE) // scale to size
            .raw()
            .toBuffer()
            .then((buffer) => {
                sys.deck.fillImage(buttonIndex, buffer);
            })
            .catch((err) => {
                console.error(err);
            });
    }
};

lib.setButton = (buttonIndex, actionObj, profileName) => {
    lib.setImage(buttonIndex, actionObj.img);
    sys.actionRegistry[buttonIndex] = actionObj;
    sys.actionRegistry[buttonIndex]._owner = profileName;
    if (actionObj && actionObj.init && typeof actionObj.init == "function") {
        actionObj.init(buttonIndex);
    }
};

lib.clearButton = (buttonIndex) => {
    sys.deck.clearKey(buttonIndex);
    sys.actionRegistry[buttonIndex] = null;
};

lib.findButton = (buttonIndex, profileName, buttonName) => {
    let owner = false;
    if (
        sys.actionRegistry[buttonIndex] &&
        sys.actionRegistry[buttonIndex]._owner == profileName &&
        sys.actionRegistry[buttonIndex].name == buttonName
    ) {
        owner = true;
    }

    return owner ? sys.actionRegistry[buttonIndex] : null;
};

/* DECK EVENTS ****************************************************************
 ******************************************************************************/
sys.deck.on("down", (keyIndex) => {
    // check if there is a down action and call it
    let keyRegistry = sys.actionRegistry[keyIndex];
    if (keyRegistry) {
        // decide repeat value
        let repeatValue = keyRegistry.repeat;
        let repeatTimer = typeof repeatValue == "number" ? repeatValue : 250;

        // create state and repeat timers
        keyRegistry._state = keyRegistry._state || {};
        keyRegistry._state.lastState = "down";
        keyRegistry._state.lastTime = Date.now();
        keyRegistry._state.timer =
            keyRegistry._state.timer ||
            setInterval(
                () => {
                    if (keyRegistry._state.lastState == "down") {
                        if (typeof keyRegistry.hold == "function") {
                            try {
                                clearInterval(keyRegistry._state.timer);
                                lib.clearState(keyRegistry);
                            } catch (err) {
                                console.log(err);
                            }
                            sys.deck.emit("hold", keyIndex);
                        } else if (keyRegistry.repeat) {
                            sys.deck.emit("down", keyIndex);
                        }
                    }
                },
                repeatTimer,
                keyRegistry
            );
    }

    // call the down action
    if (keyRegistry && typeof keyRegistry.down == "function") {
        keyRegistry.down(keyIndex);
    }

    // also call 'otherdown' on the button if a different key is pressed
    sys.actionRegistry.forEach((otherKeyIndex) => {
        if (
            otherKeyIndex &&
            typeof otherKeyIndex.otherdown == "function" &&
            otherKeyIndex !== keyIndex
        ) {
            otherKeyIndex.otherdown(otherKeyIndex);
        }
    });
});

sys.deck.on("up", (keyIndex) => {
    // check if there is an up action and call it (if we weren't holding)
    let keyRegistry = sys.actionRegistry[keyIndex];
    let wasHolding =
        keyRegistry._state && keyRegistry._state.lastState == "hold";
    lib.clearState(keyRegistry);

    if (!wasHolding) {
        if (keyRegistry) {
            keyRegistry._state = keyRegistry._state || {};
            keyRegistry._state.lastState = "up";
            keyRegistry._state.lastTime = Date.now();
            clearInterval(keyRegistry._state.timer);
        }

        if (keyRegistry && typeof keyRegistry.up == "function") {
            keyRegistry.up(keyIndex);
        }

        sys.actionRegistry.forEach((otherKeyIndex) => {
            if (
                otherKeyIndex &&
                typeof otherKeyIndex.otherup == "function" &&
                otherKeyIndex !== keyIndex
            ) {
                otherKeyIndex.otherup(otherKeyIndex);
            }
        });
    }
});

sys.deck.on("hold", (keyIndex) => {
    // check if there is a hold action and call it
    let keyRegistry = sys.actionRegistry[keyIndex];
    keyRegistry._state = keyRegistry._state || {};
    keyRegistry._state.lastState = "hold";
    keyRegistry._state.lastTime = Date.now();
    if (keyRegistry && typeof keyRegistry.hold == "function") {
        keyRegistry.hold(keyIndex);
    }

    sys.actionRegistry.forEach((otherKeyIndex) => {
        if (
            otherKeyIndex &&
            typeof otherKeyIndex.otherhold == "function" &&
            otherKeyIndex !== keyIndex
        ) {
            otherKeyIndex.otherhold(otherKeyIndex);
        }
    });
});

sys.deck.on("double", (keyIndex) => {
    // check if there is a double action and call it
    if (
        otherKeyIndex &&
        sys.actionRegistry[keyIndex] &&
        typeof sys.actionRegistry[keyIndex].double == "function"
    ) {
        sys.actionRegistry[keyIndex].double(keyIndex);
    }

    sys.actionRegistry.forEach((otherKeyIndex) => {
        if (
            typeof otherKeyIndex.otherdouble == "function" &&
            otherKeyIndex !== keyIndex
        ) {
            otherKeyIndex.otherdouble(otherKeyIndex);
        }
    });
});

sys.deck.on("error", (err) => {
    console.log(err);
});

/* INITIALIZE *****************************************************************
 ******************************************************************************/
lib.defineZone("global", [4, 9, 14]);
lib.defineZone("profile", [0, 1, 2, 5, 6, 7]);
lib.defineZone("tab", [10, 11, 12, 13]);
lib.defineZone("scroller", [3, 8]);
lib.defineProfiles();

sys.events.on("ready", () => {
    lib.assignProfileToZone("credits", "profile", 0, true);

    setTimeout(() => {
        lib.assignProfileToZone("tabs", "tab");
        lib.assignProfileToZone("global", "global");
        lib.assignProfileToZone("control", "profile");
        lib.assignProfileToZone("scrollbuttons", "scroller");
        sys.lastProfile = config.defaultProfile;
        setInterval(lib.scanActiveWindow, config.profilePollingRate);
    }, 1000);
});

function exitHandler(options, exitCode) {
    if (sys.vm && sys.vm.disconnect) {
        sys.vm.disconnect();
    }
}
//do something when app is closing
process.on("exit", exitHandler.bind(null, { cleanup: true }));
//catches ctrl+c event
process.on("SIGINT", exitHandler.bind(null, { exit: true }));
// catches "kill pid" (for example: nodemon restart)
process.on("SIGUSR1", exitHandler.bind(null, { exit: true }));
process.on("SIGUSR2", exitHandler.bind(null, { exit: true }));
