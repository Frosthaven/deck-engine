const BasicProfile = use('classes/BasicProfile');

class Profile extends BasicProfile {
	buttons() {
		return [
            {
                img: './img/shadowplay-screenshot.png',
                down: () => {
                    this.lib.ahk('shadowplay_screenshot')
                }
            },
            {
                img: './img/shadowplay-record.png',
                down: () => {
                    this.lib.ahk('shadowplay_record')
                }
            },
            {
                img: './img/shadowplay-record-last-5.png',
                down: () => {
                    this.lib.ahk('shadowplay_last_5')
                }
            },
            {
                img: './img/sharex-clip.png',
                down: () => {
                    this.lib.ahk('sharex_clip')
                }
            },
            {
                img: './img/sharex-gif.png',
                down: () => {
                    this.lib.ahk('sharex_gif')
                }
            },
            {
                img: './img/sharex-colorpicker.png',
                down: () => {
                    this.lib.ahk('sharex_colorpicker')
                }
            },
            {
                img: './img/WindowSpy.png',
                down: () => {
                    this.lib.ahk('WindowSpy')
                }
            },
        ]
	}
}

module.exports = Profile