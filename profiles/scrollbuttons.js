const BasicProfile = use('classes/BasicProfile');

class Profile extends BasicProfile {

    setup() {
        this.buttonIndexUp   = null
        this.buttonIndexDown = null
    }

    canPageUp() {
        let newOffset = this.sys.lastOffset + this.lib.getZonePagingSteps("profile")*-1
        const bounds = 0

        return (newOffset < bounds) ? false : true
    }

    canPageDown() {
        let newOffset = this.sys.lastOffset + this.lib.getZonePagingSteps("profile")*1
        const bounds = this.sys.profiles[this.sys.lastProfile].length -1

        return (newOffset > bounds) ? false : true
    }

    updateScrollUp() {
        if (this.canPageUp()) {
            this.lib.setImage(this.buttonIndexUp, `./img/scrollup.png`)
        } else {
            this.lib.setImage(this.buttonIndexUp, `./img/scrollup-disable.png`)
        }
    }

    updateScrollDown() {
        if (this.canPageDown()) {
            this.lib.setImage(this.buttonIndexDown, `./img/scrolldown.png`)
        } else {
            this.lib.setImage(this.buttonIndexDown, `./img/scrolldown-disable.png`)
        }
    }

	buttons() {
		return [
            {
                init: (x) => {
                    this.buttonIndexUp = x
                },
                windowchange: () => {
                    this.updateScrollDown()
                    this.updateScrollUp()
                },
                profilechange: () => {
                    this.updateScrollDown()
                    this.updateScrollUp()
                },
                down: () => {
                    if (this.canPageUp()) {
                        this.lib.assignProfileToZone(this.sys.lastProfile, "profile", this.lib.getZonePagingSteps("profile")*-1)
                    }
                }
            },
            {
                init: (x) => {
                    this.buttonIndexDown = x
                },
                windowchange: () => {
                    this.updateScrollDown()
                    this.updateScrollUp()
                },
                profilechange: () => {
                    this.updateScrollDown()
                    this.updateScrollUp()
                },
                down: () => {
                    if (this.canPageDown()) {
                        this.lib.assignProfileToZone(this.sys.lastProfile, "profile", this.lib.getZonePagingSteps("profile")*1)
                    }
                }
            }
        ]
	}
}

module.exports = Profile