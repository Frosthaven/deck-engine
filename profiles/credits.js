const BasicProfile = use('classes/BasicProfile');

class Profile extends BasicProfile {
	buttons() {
		return [
            {
                img: './img/credits1.png'
            },
            {
                img: './img/credits2.png'
            }
        ]
	}
}

module.exports = Profile