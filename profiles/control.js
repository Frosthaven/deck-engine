const BasicProfile = use("classes/BasicProfile");
const RateLimitedQueue = use("classes/RateLimitedQueue");
const winAudio = require("win-audio");
const speaker = winAudio.speaker;

class Profile extends BasicProfile {
    setup() {
        let self = this;
        this.buttonIndex = null;
        this.lastSpeakerImage = null;

        this.lightButtonIndex = null;
        this.lastLightImage = null;
        this.lastLightIndex = null;
        this.lightChoices = ["low", "medium", "high", "rgb", "mom"];

        this.lib.registerTask("updateSpeakerState", 500, () => {
            self.updateSpeakerIcon();
        });

        this.light_queue = new RateLimitedQueue({
            units: 1,
            per: 1000,
            max: 2,
            action: (unit) => {
                self.lib.ahk(unit.button_name, [unit.mode_name]);
            },
        });
    }

    triggerLightChange() {
        let modeName = this.lightChoices[this.lastLightIndex];
        if (!modeName) {
            modeName = "off";
        }

        this.light_queue.add({
            button_name: "smart-bulbs",
            mode_name: modeName,
        });
    }

    updateLightingIcon() {
        if (
            this.lib.findButton(
                this.lightButtonIndex,
                this._profileName,
                "bulb_icon"
            )
        ) {
            let imageName = this.lightChoices[this.lastLightIndex];
            if (!imageName) {
                imageName = "off";
            }
            let imagePath = `./img/smart-bulbs/${imageName}.png`;

            this.lastLightImage = imagePath;
            this.lib.setImage(this.lightButtonIndex, imagePath);
        }
    }

    updateSpeakerIcon() {
        if (
            this.lib.findButton(
                this.buttonIndex,
                this._profileName,
                "speaker_icon"
            )
        ) {
            let imagePath = "./img/media-mute-off.png";
            if (speaker.isMuted()) {
                imagePath = "./img/media-mute-on.png";
            }

            if (this.lastSpeakerImage !== imagePath) {
                this.lastSpeakerImage = imagePath;
                this.lib.setImage(this.buttonIndex, imagePath);
            }
        }
    }

    toggleSpeaker() {
        this.lib.ahk("media_mute");
        this.updateSpeakerIcon();
    }

    buttons() {
        return [
            {
                img: "./img/media-play-pause.png",
                down: () => {
                    this.lib.ahk("media_play_pause");
                },
            },
            {
                img: "./img/media-skip-back.png",
                down: () => {
                    this.lib.ahk("media_prev");
                },
            },
            {
                img: "./img/media-skip-forward.png",
                down: () => {
                    this.lib.ahk("media_next");
                },
            },
            {
                name: "speaker_icon",
                init: (x) => {
                    this.buttonIndex = x;
                    this.updateSpeakerIcon();
                },
                otherdown: () => {
                    this.lastSpeakerImage = null;
                    this.updateSpeakerIcon();
                },
                down: () => {
                    this.toggleSpeaker();
                },
            },
            {
                name: "bulb_rgb",
                img: "./img/smart-bulbs/rgb.png",
                init: (x) => {},
                down: (x) => {
                    this.light_queue.add({
                        button_name: "smart-bulbs",
                        mode_name: "rgb",
                    });
                },
            },
            {
                name: "bulb_mom",
                img: "./img/smart-bulbs/mom.png",
                init: (x) => {},
                down: (x) => {
                    this.light_queue.add({
                        button_name: "smart-bulbs",
                        mode_name: "mom",
                    });
                },
            },
            {
                name: "bulb_low",
                img: "./img/smart-bulbs/low.png",
                init: (x) => {},
                down: (x) => {
                    this.light_queue.add({
                        button_name: "smart-bulbs",
                        mode_name: "low",
                    });
                },
            },
            {
                name: "bulb_medium",
                img: "./img/smart-bulbs/medium.png",
                init: (x) => {},
                down: (x) => {
                    this.light_queue.add({
                        button_name: "smart-bulbs",
                        mode_name: "medium",
                    });
                },
            },
            {
                name: "bulb_high",
                img: "./img/smart-bulbs/high.png",
                init: (x) => {},
                down: (x) => {
                    this.light_queue.add({
                        button_name: "smart-bulbs",
                        mode_name: "high",
                    });
                },
            },
            {
                name: "ceiling_on",
                img: "./img/smart-bulbs/ceiling.png",
                init: (x) => {},
                down: (x) => {
                    this.lib.ahk("smart-bulbs", ["ceiling-on"]);
                },
            },
            {
                name: "ceiling_off",
                img: "./img/smart-bulbs/ceiling_off.png",
                init: (x) => {},
                down: (x) => {
                    this.lib.ahk("smart-bulbs", ["ceiling-off"]);
                },
            },
            {
                name: "bulb_off",
                img: "./img/smart-bulbs/off.png",
                init: (x) => {},
                down: (x) => {
                    this.light_queue.add({
                        button_name: "smart-bulbs",
                        mode_name: "off",
                    });
                },
            },
            /*
            {
                name: "bulb_icon",
                init: (x) => {
                    this.lightButtonIndex = this.lightButtonIndex || x
                    this.lastLightIndex = this.lastLightIndex || 3
                    this.updateLightingIcon()

                    if (!this.firstLightInit) {
                        this.firstLightInit = true
                        this.triggerLightChange()
                    }
                },
                up: (x) => {
                    // go to the next in the sequence
                    let maxIndex = this.lightChoices.length - 1
                    this.lastLightIndex++
                    if (this.lastLightIndex > maxIndex) {
                        this.lastLightIndex = 0
                    }
                    this.updateLightingIcon()
                    this.triggerLightChange()
                },
                hold: () => {
                    if (this.lastLightIndex == -1) {
                        this.lastLightIndex = this.savedLightIndex
                    } else {
                        this.savedLightIndex = this.lastLightIndex
                        this.lastLightIndex = -1
                    }
                    this.updateLightingIcon()
                    this.triggerLightChange()
                },
                otherdown: () => {
                    this.updateLightingIcon()
                }
            },
            {
                img: './img/icue-random.png',
                down: () => {
                    this.lib.ahk('icue_random')
                }
            },
            */
            {
                img: "./img/icue-previous.png",
                down: () => {
                    this.lib.ahk("icue_previous");
                    setTimeout(() => {
                        this.lib.ahk("smart-bulbs", ["rgb"]);
                    }, 500);
                },
            },
            {
                img: "./img/icue-next.png",
                down: () => {
                    this.lib.ahk("icue_next");
                    setTimeout(() => {
                        this.lib.ahk("smart-bulbs", ["rgb"]);
                    }, 500);
                },
            },
            {
                img: "./img/rgb-profiles/aphrodite.png",
                down: () => {
                    this.lib.ahk("icue_select", ["CRGB_Hades_Aphrodite"]);
                    this.lib.ahk("smart-bulbs", ["rgb"]);
                },
            },
            {
                img: "./img/rgb-profiles/alecto.png",
                down: () => {
                    this.lib.ahk("icue_select", ["CRGB_Hades_Alecto"]);
                    this.lib.ahk("smart-bulbs", ["rgb"]);
                },
            },
            {
                img: "./img/rgb-profiles/artemis.png",
                down: () => {
                    this.lib.ahk("icue_select", ["CRGB_Hades_Artemis"]);
                    this.lib.ahk("smart-bulbs", ["rgb"]);
                },
            },
            {
                img: "./img/rgb-profiles/chaos.png",
                down: () => {
                    this.lib.ahk("icue_select", ["CRGB_Hades_Chaos"]);
                    this.lib.ahk("smart-bulbs", ["rgb"]);
                },
            },
            {
                img: "./img/rgb-profiles/dionysus.png",
                down: () => {
                    this.lib.ahk("icue_select", ["CRGB_Hades_Dionysus"]);
                    this.lib.ahk("smart-bulbs", ["rgb"]);
                },
            },
            {
                img: "./img/rgb-profiles/hermes.png",
                down: () => {
                    this.lib.ahk("icue_select", ["CRGB_Hades_Hermes"]);
                    this.lib.ahk("smart-bulbs", ["rgb"]);
                },
            },
            {
                img: "./img/rgb-profiles/megaera.png",
                down: () => {
                    this.lib.ahk("icue_select", ["CRGB_Hades_Megaera"]);
                    this.lib.ahk("smart-bulbs", ["rgb"]);
                },
            },
            {
                img: "./img/rgb-profiles/poseidon.png",
                down: () => {
                    this.lib.ahk("icue_select", ["CRGB_Hades_Poseidon"]);
                    this.lib.ahk("smart-bulbs", ["rgb"]);
                },
            },
            {
                img: "./img/rgb-profiles/skelly.png",
                down: () => {
                    this.lib.ahk("icue_select", ["CRGB_Hades_Skelly"]);
                    this.lib.ahk("smart-bulbs", ["rgb"]);
                },
            },
            {
                img: "./img/rgb-profiles/thanatos.png",
                down: () => {
                    this.lib.ahk("icue_select", ["CRGB_Hades_Thanatos"]);
                    this.lib.ahk("smart-bulbs", ["rgb"]);
                },
            },
            {
                img: "./img/rgb-profiles/tisiphone.png",
                down: () => {
                    this.lib.ahk("icue_select", ["CRGB_Hades_Tisiphone"]);
                    this.lib.ahk("smart-bulbs", ["rgb"]);
                },
            },
            {
                img: "./img/rgb-profiles/zagreus.png",
                down: () => {
                    this.lib.ahk("icue_select", ["CRGB_Hades_Zagreus"]);
                    this.lib.ahk("smart-bulbs", ["rgb"]);
                },
            },
            {
                img: "./img/rgb-profiles/zeus.png",
                down: () => {
                    this.lib.ahk("icue_select", ["CRGB_Hades_Zeus"]);
                    this.lib.ahk("smart-bulbs", ["rgb"]);
                },
            },
        ];
    }
}

module.exports = Profile;
