const BasicProfile = use("classes/BasicProfile");
const winAudio = require("win-audio");
// const Speaker = winAudio.speaker;
// const StripProperties = require("voicemeeter-connector").StripProperties;
// const BusProperties = require("voicemeeter-connector").BusProperties;
const mic = winAudio.mic;

class Profile extends BasicProfile {
    setup() {
        let self = this;
        this.buttonIndex = null;
        this.lastMicImage = null;
        this.lastVolume = null;
        this.lastMute = false;

        this.lib.registerTask("update-mic-and-vm-gain", 500, () => {
            self.updateMicIcon();
        });
    }

    updateMicIcon() {
        if (
            this.lib.findButton(this.buttonIndex, this._profileName, "mic_icon")
        ) {
            let imagePath = "./img/mic-on.png";
            if (mic.isMuted()) {
                imagePath = "./img/mic-off.png";
            }

            if (this.lastMicImage !== imagePath) {
                this.lastMicImage = imagePath;
                this.lib.setImage(this.buttonIndex, imagePath);
            }
        }
    }

    toggleMic() {
        if (mic.isMuted()) {
            mic.unmute();
            if (this.sys.isVoiceMeterEnabled()) {
                this.sys.vm.setParameter(
                    this.config.vm.mic.type,
                    this.config.vm.mic.index,
                    "Mute",
                    0
                );
            }
        } else {
            mic.mute();
            if (this.sys.isVoiceMeterEnabled()) {
                this.sys.vm.setParameter(
                    this.config.vm.mic.type,
                    this.config.vm.mic.index,
                    "Mute",
                    1
                );
            }
        }
        this.updateMicIcon();
    }

    buttons() {
        return [
            {
                img: "./img/volume-up.png",
                repeat: 75,
                down: () => {
                    this.lib.ahk("volume_up");
                },
            },
            {
                img: "./img/volume-down.png",
                repeat: 75,
                down: () => {
                    this.lib.ahk("volume_down");
                },
            },
            {
                name: "mic_icon",
                otherdown: () => {
                    this.lastMicImage = null;
                    this.updateMicIcon();
                },
                init: (x) => {
                    this.buttonIndex = x;
                    this.updateMicIcon();
                },
                down: () => {
                    this.toggleMic();
                },
            },
        ];
    }
}

module.exports = Profile;
