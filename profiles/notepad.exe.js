const BasicProfile = use('classes/BasicProfile');

class Profile extends BasicProfile {
	buttons() {
		return [
			{
				img: './img/xiv/Basic_Synthesis_(Weaver)_Icon.png',
				down: (x) => {
					this.lib.ahk('xiv/craft')
				}
			},
			{
				img: './img/xiv/Aetherial_Reduction_Icon.png',
				down: (x) => {
					this.lib.ahk('xiv/desynth')
				}
			},
			{
				img: './img/xiv/meters.png',
				down: (x) => {
					this.lib.ahk('xiv/meters')
				}
			},
			{
				img: './img/xiv/timers.png',
				down: (x) => {
					this.lib.ahk('xiv/timers')
				}
			},
			{
				img: './img/xiv/gshade_menu.png',
				down: (x) => {
					this.lib.ahk('xiv/gshade_menu')
				}
			},
			{
				img: './img/xiv/gshade.png',
				down: (x) => {
					this.lib.ahk('xiv/gshade')
				}
			}
		]
	}
}

module.exports = Profile
