const BasicProfile = use("classes/BasicProfile");
const StripProperties = require("voicemeeter-connector").StripProperties;
const BusProperties = require("voicemeeter-connector").BusProperties;

class Profile extends BasicProfile {
    setup() {
        let self = this;

        this.buttonIndexBrowser = null;
        this.buttonIndexMusic = null;

        this.isSharingBrowser = false;
        this.isSharingMusic = false;

        this.img = {
            browser: {
                on: "./img/share-audio-browser-on.png",
                off: "./img/share-audio-browser-off.png",
            },
            music: {
                on: "./img/share-audio-music-on.png",
                off: "./img/share-audio-music-off.png",
            },
        };

        this.lib.registerTask("update-audio-share-icons", 1500, () => {
            self.updateBrowserShareImage(true);
            self.updateMusicShareImage(true);
        });
    }

    // BROWSER SHARING ***********************************************

    isBrowserSharing() {
        let isBrowserSharing = this.sys.vm.getStripParameter(
            2,
            StripProperties.B1
        );

        return isBrowserSharing === 1 ? true : false;
    }
    updateBrowserShareImage(fromRemoteValue = false) {
        if (
            this.lib.findButton(
                this.buttonIndexBrowser,
                this._profileName,
                "browser_share_icon"
            )
        ) {
            if (fromRemoteValue) {
                this.isSharingBrowser = this.isBrowserSharing();
            }

            let imagePath = this.isSharingBrowser
                ? this.img.browser.on
                : this.img.browser.off;
            this.lib.setImage(this.buttonIndexBrowser, imagePath);
        }
    }

    toggleBrowserShare() {
        if (!this.isSharingBrowser && this.sys.isVoiceMeterEnabled()) {
            this.sys.vm.setParameter(
                this.config.vm.browser.type,
                this.config.vm.browser.index,
                this.config.vm.browser.mixTo,
                1
            );
            this.isSharingBrowser = true;
        } else if (this.sys.isVoiceMeterEnabled()) {
            this.sys.vm.setParameter(
                this.config.vm.browser.type,
                this.config.vm.browser.index,
                this.config.vm.browser.mixTo,
                0
            );
            this.isSharingBrowser = false;
        }
        this.updateBrowserShareImage();
    }

    // MUSIC SHARING *************************************************

    isMusicSharing() {
        let isMusicSharing = this.sys.vm.getStripParameter(
            3,
            StripProperties.B1
        );

        return isMusicSharing === 1 ? true : false;
    }
    updateMusicShareImage(fromRemoteValue = false) {
        if (
            this.lib.findButton(
                this.buttonIndexMusic,
                this._profileName,
                "music_share_icon"
            )
        ) {
            if (fromRemoteValue) {
                this.isSharingMusic = this.isMusicSharing();
            }

            let imagePath = this.isSharingMusic
                ? this.img.music.on
                : this.img.music.off;
            this.lib.setImage(this.buttonIndexMusic, imagePath);
        }
    }

    toggleMusicShare() {
        if (!this.isSharingMusic && this.sys.isVoiceMeterEnabled()) {
            this.sys.vm.setParameter(
                this.config.vm.music.type,
                this.config.vm.music.index,
                this.config.vm.music.mixTo,
                1
            );
            this.isSharingMusic = true;
        } else if (this.sys.isVoiceMeterEnabled()) {
            this.sys.vm.setParameter(
                this.config.vm.music.type,
                this.config.vm.music.index,
                this.config.vm.music.mixTo,
                0
            );
            this.isSharingMusic = false;
        }
        this.updateMusicShareImage();
    }

    buttons() {
        return [
            {
                name: "browser_share_icon",
                otherdown: () => {
                    this.updateBrowserShareImage();
                },
                init: (x) => {
                    this.buttonIndexBrowser = x;
                    this.updateBrowserShareImage();
                },
                down: () => {
                    this.toggleBrowserShare();
                },
            },
            {
                name: "music_share_icon",
                otherdown: () => {
                    this.updateMusicShareImage();
                },
                init: (x) => {
                    this.buttonIndexMusic = x;
                    this.updateMusicShareImage();
                },
                down: () => {
                    this.toggleMusicShare();
                },
            },
            {
                img: "./img/studio-stream.png",
            },
            {
                img: "./img/studio-record.png",
            },
            {
                img: "./img/studio-mic.png",
            },
            {
                img: "./img/studio-scene-1.png",
            },
            {
                img: "./img/studio-scene-2.png",
            },
            {
                img: "./img/studio-scene-3.png",
            },
            {
                img: "./img/studio-scene-4.png",
            },
        ];
    }
}

module.exports = Profile;
