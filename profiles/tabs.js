const BasicProfile  = use('classes/BasicProfile');
const fs            = require('fs');
const path          = require('path');
const fileIcon      = require('extract-file-icon');

class Profile extends BasicProfile {

    setup() {
        this.buttonIndex = null
    }

    updateProcessAvailable(exePath, iconBuffer) {
        let imgName = './img/tab-current-disable.png'
        if (this.sys.profiles[this.sys.lastProcess]) {
            if (iconBuffer) {
                let png = path.normalize(`./cache/${this.sys.lastProcess}.png`)
                if (!fs.existsSync(png)) {
                    fs.writeFileSync(png,iconBuffer,"binary",((err) => console.log ));
                }
                this.lib.setImage(this.buttonIndex, png)
            } else {
                imgName = './img/tab-current.png'
                this.lib.setImage(this.buttonIndex,imgName)
            }
        } else {
            this.lib.setImage(this.buttonIndex,imgName)
        }
    }

	buttons() {
		return [
            {
                img: './img/tab-media.png',
                down: () => {
                    this.sys.lastOffset=0
                    this.lib.assignProfileToZone("control", "profile",0,true)
                }
            },
            {
                img: './img/tab-capture.png',
                down: () => {
                    this.sys.lastOffset=0
                    this.lib.assignProfileToZone("capture","profile",0,true)
                }
            },
            {
                img: './img/tab-obs.png',
                down: () => {
                    this.sys.lastOffset=0
                    this.lib.assignProfileToZone("studio", "profile",0,true)
                }
            },
            {
                img: './img/tab-current.png',
                init: (x) => {
                    let self = this;
                    this.buttonIndex = x
                    this.updateProcessAvailable(this.buttonIndex)
                },
                windowchange: (window) => {
                    const exePath = window.owner.path
                    if (exePath) {
                        this.updateProcessAvailable(
                            this.buttonIndex,
                            fileIcon(exePath, this.sys.deck.ICON_SIZE)
                        )
                    } else {
                        this.updateProcessAvailable(this.buttonIndex)
                    }
                },
                down: () => {
                    if (this.sys.profiles[this.sys.lastProcess]) {
                        this.sys.lastOffset=0
                        this.lib.assignProfileToZone(this.sys.lastProcess, "profile",0,true)
                    }
                }
            },
        ]
	}
}

module.exports = Profile